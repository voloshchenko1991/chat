package com.levelup.socket.client.visual;

import com.levelup.utils.SocketUtils;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Danyl on 012 12.09.17.
 */
public class Controller {

    private PrintWriter printWriter;

    private String name;

    private String address;

    @FXML
    TextArea txtChat;

    @FXML
    TextField txtInput;

    @FXML
    TextField txtName;

    @FXML
    TextField txtAddress;

    @FXML
    Button btnSend;

    @FXML
    Button btnConnection;

    @FXML
    private void getConnection() {

        name = txtName.getText();

        address = txtAddress.getText();

        if ((name.length() > 0) && (address.length() > 0)) {
//            SocketClient socketClient = new SocketClient(name);
//            socketClient.connect(address);

            btnSend.setDefaultButton(true);
            btnConnection.setDefaultButton(false);
            txtInput.requestFocus();

            try {
                Socket socket = new Socket(address, 8080);
                printWriter = new PrintWriter(socket.getOutputStream());
                Thread thread =  new Thread(() -> {
                    try {
                        BufferedReader bufferedReader = new BufferedReader((new InputStreamReader(socket.getInputStream())));
                        while (true) {
                            String line;
                            while ((line = bufferedReader.readLine()) != null) {
                                txtChat.appendText(line);
                                txtChat.appendText("\r\n");
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

                thread.setDaemon(true);
                thread.start();

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void sendMessage(){
        if(txtInput.getText().length() > 0) {
            SocketUtils.sendMessage(name + ": " + txtInput.getText(), printWriter);
            txtInput.clear();
        }
    }

    @FXML
    public void initialize(){
        btnConnection.setDefaultButton(true);
        Platform.runLater(()->txtName.requestFocus());
    }
}
