package com.levelup.socket.client.terminal;

import com.levelup.utils.SocketUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketClient {

    private PrintWriter printWriter;
    private String clientName;
    public SocketClient(String clientName){
        this.clientName = clientName;
    }

    public void connect(String address) {
        try  {
            Socket socket = new Socket(address, 8080);
            printWriter = new PrintWriter(socket.getOutputStream());
            new Thread(()->{
                try {
                    BufferedReader bufferedReader = new BufferedReader((new InputStreamReader(socket.getInputStream())));
                    while(true){
                        String line;
                        while((line = bufferedReader.readLine())!= null){
                            System.out.println(line);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }).start();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void sendMessage(String message){
        SocketUtils.sendMessage(clientName + ": " + message, printWriter);
    }


}
