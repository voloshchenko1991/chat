package com.levelup.socket.client.terminal;

import java.io.IOException;
import java.util.Scanner;

public class MainClient {


    public static void main(String[] args) throws IOException {
        SocketClient socketClient = new SocketClient("Danyl");
        socketClient.connect("127.0.0.1");
        Scanner scanner  = new Scanner(System.in);
        String line;
        while(true){
            line = scanner.nextLine();

            if(line.length() > 0){
                socketClient.sendMessage(line);
            }

        }



    }
}
