package com.levelup.socket.server;

import com.levelup.utils.SocketUtils;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

@Service
public class SocketServer {

    private List<PrintWriter> printWriterList = new ArrayList<>();
    private List<String> history = new ArrayList<>();
    public void start() {
        try  {
            ServerSocket serverSocket = new ServerSocket(8080);
            System.out.println("Listening");

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("New user connected");
                new Thread(() -> {
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
                        printWriterList.add(printWriter);
                        history.forEach(a -> SocketUtils.sendMessage(a.toString(),printWriter));
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            System.out.println(line);
                            history.add(line);
                            for(int i = 0; i < printWriterList.size(); i++){
                                SocketUtils.sendMessage(line, printWriterList.get(i));
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();
            }

        } catch (
                IOException e)

        {
            e.printStackTrace();
        }
    }
}

