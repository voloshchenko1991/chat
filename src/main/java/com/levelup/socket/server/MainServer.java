package com.levelup.socket.server;

import com.levelup.socket.server.SocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
public class MainServer {

    public static void main(String[] args) throws IOException {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("main.xml");
        SocketServer socketServer = (SocketServer)applicationContext.getBean("socketServer");
        socketServer.start();
    }
}
